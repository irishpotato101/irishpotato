import turtle

def beef():
	turtle.TurtleScreen._RUNNING = True
	t = turtle.Turtle()
	w = turtle.Screen()
	#turtle.tracer(0, 0)
	t.pencolor('#ad3bcc')
	t.fillcolor('#FFFFFF')
	t.goto(-200,150)
	t.begin_fill()
	t.rt(300)
	t.forward(90)
	t.rt(45)
	t.goto(90,250)
	t.rt(70)
	t.forward(90)
	t.goto(0,0)
	t.end_fill()


	t.penup()
	t.goto(-50,40)
	t.rt(300)
	t.pendown()
	t.rt(12)
	t.forward(70)
	t.rt(-15)
	t.forward(5)
	t.rt(-15)
	t.rt(-15)
	t.forward(5)
	t.rt(-10)
	t.forward(10)
	t.rt(-5)
	t.forward(10)
	t.rt(-1)
	t.forward(10)
	t.rt(-1)
	t.forward(10)
	t.rt(-1)
	t.forward(15)
	t.rt(-15)
	t.rt(-10)
	t.forward(15)
	t.rt(-10)
	t.forward(1)
	t.rt(-10)
	t.forward(1)
	t.rt(-10)
	t.forward(1)
	t.rt(-15)
	t.forward(10)
	t.rt(-15)
	t.forward(10)
	t.rt(-15)
	t.forward(10)
	t.rt(-15)
	t.forward(10)
	t.rt(-15)
	t.forward(10)
	t.forward(30)
	t.forward(30)
	t.forward(30)
	t.forward(30)
	t.rt(15)
	t.forward(1)
	t.rt(15)
	t.forward(1)
	t.forward(15)
	t.rt(15)
	t.forward(15)
	t.forward(15)
	t.rt(15)
	t.forward(15)
	t.forward(15)
	t.rt(15)
	t.rt(15)
	t.rt(15)
	t.forward(10)
	t.forward(10)
	t.rt(15)
	t.forward(10)
	t.rt(15)
	t.forward(10)
	t.rt(15)
	t.forward(10)
	t.rt(15)
	t.forward(10)
	t.rt(15)
	t.forward(10)
	t.forward(30)
	t.forward(30)
	t.forward(30)
	t.forward(30)
	t.rt(15)
	t.forward(10)
	t.rt(15)
	t.forward(10)

	w.exitonclick()


def main():
	beef()


if __name__ == '__main__':
	main()
